var Shops = require('mongoose').model('Shops'),
    passport = require('passport');

exports.read = function(req, res) {
    res.json(req.shops);
};


exports.create = function(req,res,next)
{   
    var shopsdata = new Shops(req.body);
    shopsdata.save(function(err){
        if(err){
            return(next(err));
        } else {
            res.json(shopsdata)
        }
    });
};


exports.update = function(req, res, next) 
{
    Score.findByIdAndUpdate(req.shops._id, req.body, function(err, shops) 
    {
        if (err) {
            return next(err);
        }
        else {
            res.json(shops);
        }
    });
};


exports.delete = function(req, res, next) 
{
   Shops.remove({ shops_id: req.params.shopId }, function(err) {
        if (err) {
            res.json({ok:false, message:"Error, there was error to delete the shop"});
        }
        else {
            res.json({ok:true, message:"Shop has been deleted successfully"});
        }
   });
};

exports.deleteByCategory = function(req, res, next) 
{
   Shops.remove({ category_id: req.params.categoryId }, function(err) {
        if (err) {
            res.json({ok:false, message:"Error, there was error to delete the shop"});
        }
        else {
            res.json({ok:true, message:"Shop has been deleted successfully"});
        }
   });
};



exports.list = function(req, res, next) 
{
    Shops.find({}, function(err, shops){
        if (err) {
            return next(err);
        }
        else {
            res.json(shops);
        }
    });
};



exports.shopsByID = function(req, res, next, id) 
{
     Shops.findOne({
            _id: id
        },
        function(err, res) {
            if (err) {
                return next(err);
            }
            else {
                req.shops = res;
                next();
            }
        }
    );
};
