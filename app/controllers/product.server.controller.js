var Product = require('mongoose').model('Product'),
    passport = require('passport');

exports.read = function(req, res) {
    res.json(req.product);
};


exports.create = function(req,res,next)
{   
    var productdata = new Product(req.body);
    productdata.save(function(err){
        if(err){
            return(next(err));
        } else {
            res.json(productdata)
        }
    });
};


exports.update = function(req, res, next) 
{
    Product.findByIdAndUpdate(req.product._id, req.body, function(err, products) 
    {
        if (err) {
            return next(err);
        }
        else {
            res.json(products);
        }
    });
};


exports.delete = function(req, res, next) 
{

   console.log(req.params);
   Product.remove({ product_id: req.params.productId }, function(err) {
        if (err) {
            res.json({ok:false, message:"Error, there was error to delete the product"});
        }
        else {
            res.json({ok:true, message:"Product has been deleted successfully"});
        }
   });
   
};


exports.list = function(req, res, next) 
{
    Product.find({}, function(err, products){
        if (err) {
            return next(err);
        }
        else {
            res.json(products);
        }
    });
};



exports.productDataByID = function(req, res, next, id) 
{
     Product.findOne({
            _id: id
        },
        function(err, products) {
            if (err) {
                return next(err);
            }
            else {
                req.product = products;
                next();
            }
        }
    );
};
