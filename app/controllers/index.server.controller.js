exports.render = function(req, res) {
    res.render('index', {
    	title: 'ShopVR360 API',
		user: req.user ? req.user.username : ''

    });
};  