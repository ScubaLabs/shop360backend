var Store = require('mongoose').model('Store'),
    passport = require('passport');

exports.read = function(req, res) {
    res.json(req.product);
};


exports.create = function(req,res,next)
{   
    var storedata = new Store(req.body);
    storedata.save(function(err){
        if(err){
            return(next(err));
        } else {
            res.json(storedata)
        }
    });
};


exports.update = function(req, res, next) 
{
    Store.findByIdAndUpdate(req.store._id, req.body, function(err, products) 
    {
        if (err) {
            return next(err);
        }
        else {
            res.json(products);
        }
    });
};


exports.delete = function(req, res, next) 
{

   Store.remove({ merchantId: req.params.merchantId }, function(err) {
        if (err) {
            res.json({ok:false, message:"Error, there was error to delete the product"});
        }
        else {
            res.json({ok:true, message:"Store has been deleted successfully"});
        }
   });
   
};


exports.list = function(req, res, next) 
{
    Store.find({merchantId:req.params.merchantId}, function(err, storedata){
        if (err) {
            return next(err);
        }
        else {
            res.json(storedata);
        }
    });
};

