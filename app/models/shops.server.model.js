var mongoose = require('mongoose'),
 	crypto = require('crypto'),
    Schema = mongoose.Schema;

var ScoreSchema = new Schema({
    category_name: String,
    category_id: String,
    shops_id: String,
    created_at: {type: Date, required: true, default: Date},
    updated_at: {type: Date, required: true, default: Date},
    isactive:   {type: Boolean, default: true}
},
{
    versionKey: false //Setting document version to false
}
);

mongoose.model('Shops', ScoreSchema);