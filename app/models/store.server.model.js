var mongoose = require('mongoose'),
 	crypto = require('crypto'),
    Schema = mongoose.Schema;

var StoreSchema = new Schema({
    shopId: String,
    merchantId: String,
    adminPassword: String,
    merchantPassword: String,
    sessionIdUrl: String,
    createUserUrl: String,
    loginCustomerUrl: String,
    mainCategoryUrl: String,
    baseCategoryUrl: String,
    subCategoryUrl: String,
    categoryByDepthUrl: String,
    allProductByIdUrl: String,
    bestSellingProductsUrl: String,
    cartUrl: String,
    productByIdUrl: String,
    emptyCartUrl: String,
    addToCartBulkUrl: String,
    loginAgainUrl: String,
    payementAddressUrl: String,
    shippingAddressUrl: String,
    createGuestUrl: String,
    guestShippingUrl: String,
    shippingMethodsUrl: String,
    paymentMethodUrl: String,
    confirmOverviewUrl: String,
    confirmUrl: String,
    addANewProductUrl: String,
    shopDataOnServerUrl: String,
    productDataOnServerUrl: String,
    deleteshopsbycategoryUrl: String,
    created_at: {type: Date, required: true, default: Date},
    updated_at: {type: Date, required: true, default: Date},
    isactive:   {type: Boolean, default: true}
},
{
    versionKey: false //Setting document version to false
}
);

mongoose.model('Store', StoreSchema);