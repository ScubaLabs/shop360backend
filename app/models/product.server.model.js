var mongoose = require('mongoose'),
 	crypto = require('crypto'),
    Schema = mongoose.Schema;

var ScoreSchema = new Schema({
    product_id: String,
    category_id: String,
    product_location: String,
    shop_id: String,
    model_id: String,
    suggested:   {type: Boolean, default: false},
   //position:   { type : Array , "default" : [] },
    rotation:   { type : Array , "default" : [] },
    scale:      { type : Array , "default" : [] },
    created_at: {type: Date, required: true, default: Date},
    updated_at: {type: Date, required: true, default: Date},
    isactive:   {type: Boolean, default: true}
},
{
    versionKey: false //Setting document version to false
}
);

mongoose.model('Product', ScoreSchema);