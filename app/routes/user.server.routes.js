var users = require('../../app/controllers/user.server.controller');

module.exports = function(app){
	app.route('/users').post(users.create);
	app.route('/users').get(users.list);
	app.route('/users/:userId').get(users.read);
	//app.route('/users/:userId').put(users.update);
	app.route('/users/:userId').post(users.update);
	app.route('/users/:userId').delete(users.delete);

	app.param('userId', users.userByID);

  	app.route('/register').post(users.register);
  	app.route('/login').post(passport.authenticate('local'));

    app.get('/logout', users.logout);
};