var shops = require('../../app/controllers/shops.server.controller');

module.exports = function(app){
	
	app.route('/shops').post(shops.create);
	app.route('/shops').get(shops.list);
	app.route('/shops/:shopId').get(shops.read);
	app.route('/shops/:shopId').put(shops.update);
	app.route('/shops/:shopId').delete(shops.delete);
	app.route('/deleteshopsbycategory/:categoryId').delete(shops.deleteByCategory);
	//app.param('shopId', shops.shopByID);

};