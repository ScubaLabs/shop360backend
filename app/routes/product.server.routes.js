var product = require('../../app/controllers/product.server.controller');

module.exports = function(app){
	
	app.route('/product').post(product.create);
	app.route('/product').get(product.list);
	app.route('/product/:productId').get(product.read);
	app.route('/product/:productId').put(product.update);
	app.route('/product/:productId').delete(product.delete);
	//app.param('productId', product.productByID);

};