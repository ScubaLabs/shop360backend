var store = require('../../app/controllers/store.server.controller');

module.exports = function(app){
	
	app.route('/store').post(store.create);
	//app.route('/store').get(store.list);
	app.route('/store/:merchantId').get(store.list);
	app.route('/store/:merchantId').put(store.update);
	app.route('/store/:merchantId').delete(store.delete);
	

};